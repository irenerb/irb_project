Polymer({
  /* global console */
  /**
   * Fires an event after the form has been validated
   * with the form data (user, password, reminded status)
   * @event cells-basic-login-success
   */

  /**
   * Fires an event if form is not correct
   * @event cells-basic-login-error
   */

  /**
   * Fires an event on click of forgotten password
   * @event cells-basic-login-forgotten-password
   */

  /**
   * Fires an event if reminded user is cleared to intro another one
   * @event cells-basic-login-clear-reminded
   */

  is: 'cells-basic-login',

  behaviors: [ CellsBehaviors.i18nBehavior ],

  properties: {
    /*
    * Username
    * @type {String}
    */
    username: {
      type: String,
      notify: true
    },
    /*
    * Password
    * @type {String}
    */
    password: {
      type: String,
      notify: true
    },
    /*
    * Name to show remind user
    * @type {String}
    */
    name: {
      type: String
    },
    /**
     * True if the user nif is saved
     * @type {Boolean}
     */
    savedUser: {
      type: Boolean,
      value: false,
      notify: true
    },
    /*
     * User has checked "remember" me option
     * @type {Boolean}
     */
    userReminded: {
      type: Boolean,
      value: false,
      notify: true
    },
    /*
    * Disabled validation to login
    * @type {Boolean}
    */
    disabledValidation: {
      type: Boolean
    },
    /*
    * Disabled button form
    * @type {Boolean}
    */
    disabledSubmit: {
      type: Boolean
    },
    /**
     * ID of the checked icon of `cells-switch`
     */
    iconChecked: {
      type: String,
      value: ''
    },
    /**
     * ID of the unchecked icon of `cells-switch`
     */
    iconUnchecked: {
      type: String,
      value: ''
    },
    /**
     * ID of the toggling icon of `cells-switch`
     */
    iconToggle: {
      type: String,
      value: ''
    }
  },

  ready: function() {
    this.addEventListener('click', function() {
    //  this._onCheckChanged();
    });
  },

  /**
   * Not remembering user
   * @param {Event} e Data of event
   * @private
   */
  _notRemind: function(e) {
    e.preventDefault();
    this.reset();
    this.dispatchEvent(new CustomEvent('cells-basic-login-clear-reminded', {
      bubbles: true,
      composed: true
    }));
  },
  /**
   * Validates the form and fires 'login' event with the form params
   * @param {Event} ev Data of event when form submitted
   * @private
   */
  _formSubmit: function(ev) {
    ev.preventDefault();
    console.log("(cells-basic-login) Dentro de _formSubmit");

    //if (this.disabledValidation || this.$.form.checkValidity()) {
    if (this.username && this.password) {
        this.dispatchEvent(new CustomEvent('cells-basic-login-success', {
        bubbles: true,
        composed: true,
        detail: {
          user: this.username,
          password: this.password
        }}));
        console.log("Emitido=cells-basic-login-success");
        this.reset();

    } else {
      this.dispatchEvent(new CustomEvent('cells-basic-login-error', {
        bubbles: true,
        composed: true
      }));
    }
  },
  /**
   * update user reminder
   * @param {Object} payload Data of event when checked reminder toggle
   * @private
   */
  _onCheckChanged: function(payload) {
    this.set('userReminded', payload.detail);
  },
  /**
   * Fires forgotten password event
   * @private
   */
  _forgottenPasswordAction: function(e) {
    this.dispatchEvent(new CustomEvent('cells-basic-login-forgotten-password', {
      bubbles: true,
      composed: true
    }));
  },
  /**
   * Reset Login
   */
  reset: function() {
    this.set('username', '');
    this.set('password', '');
    this.set('savedUser', false);
    this.set('userReminded', false);
  }
});
