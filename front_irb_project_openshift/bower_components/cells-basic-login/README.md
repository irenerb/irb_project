![cells-basic-login screenshot](cells-basic-login.png)

# cells-basic-login

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)

[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-basic-login)

`<cells-basic-login>` is a login form with __forgotten password__ and __remind user__ possibilities.

Basic example:
```html
<cells-basic-login icon-checked="coronita:checkmark" icon-unchecked="coronita:close"
  icon-toggle="coronita:menu"></cells-basic-login>
```

Login with saved user example:
```html
<cells-basic-login icon-checked="coronita:checkmark" icon-unchecked="coronita:close"
  icon-toggle="coronita:menu" name="Francisco" saved-user user-reminded></cells-basic-login>
```

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|-----------------|-----------------|:--------------:|
| --cells-basic-login-inputs | Mixin applied to inputs | {} |
| --cells-basic-login-inputs-label | Mixin applied to inputs' labels  | {} |
| --cells-basic-login-inputs-floated-label | Mixin applied to inputs' floated labels  | {} |
| --cells-basic-login-inputs-has-content | Mixin applied to inputs' with content  | {} |
| --cells-basic-login-inputs-icon-color | Color of inputs' icons  | #006EC1 |
| --cells-basic-login-inputs-icon | Mixin applied to inputs' icons  | {} |
| --cells-basic-login-inputs-input | Mixin applied to inputs' native input  | {} |
| --cells-basic-login-inputs-focused-color | Color of focused inputs  | #FFFAEB |
| --cells-basic-login-inputs-disabled | Mixin applied to disabled inputs | {} |
| --cells-basic-login-bg-color | Background color | #fff |
| --cells-basic-login | Mixin applied to :host | {} |
| --cells-basic-login-inputs | Mixin applied to all inputs | {} |
| --cells-basic-login-inputs-active | Mixin applied to all :active inputs | {} |
| --cells-basic-login-welcome-border-bottom-color | Border bottom color of welcome section | #E5E5E5 |
| --cells-basic-login-user-msg | Mixin applied to user message section | {} |
| --cells-basic-login-user-greeting | Mixin applied to user greeting section | {} |
| --cells-basic-login-user-name | Mixin applied to user name section | {} |
| --cells-basic-login-form-border-bottom-color | Border bottom color of form section | #E5E5E5 |
| --cells-basic-login-remember | Mixin applied to remember section | {} |
| --cells-basic-login-remember-toggle-button | Mixin applied to toggle button of remember section | {} |
| --cells-basic-login-remember-first-child | Mixin applied to first element of remember section | {} |
| --cells-basic-login-remember-label-color | Color of remember's label | #717780 |
| --cells-basic-login-remember-label | Mixin applied to remember's label | {} |
| --cells-basic-login-links-color | Color of links | #006EC1 |
| --cells-basic-login-links | Mixin applied to links | {} |
| --cells-basic-login-forgot-pwd | Mixin applied to forgotten password section | {} |
| --cells-basic-login-stripes-bar | Mixin applied to stripes bar (savedUser: true) | {} |
| --cells-basic-login-layout-row-center | Mixin applied to part of welcome section (savedUser: true) | {} |

## Public methods

* reset(): Resets login form

## Events

* `cells-basic-login-success`: Fires an event after the form has been validated with the form data (user, password, reminded status)
* `cells-basic-login-error`: Fires an event if form is not correct
* `cells-basic-login-forgotten-password`: Fires an event on click of forgotten password
* `cells-basic-login-clear-reminded`: Fires an event if reminded user is cleared to intro another one
