# \<frontirbproject-app\>
-------------------------------------------------------------------------------
Esta versión FRONT ejecuta las llamadas a la API irb_project/v1 que está operativa en OpenShift
http://irbank-route-irb-project.a3c1.starter-us-west-1.openshiftapps.com/irb_project/v1/
-------------------------------------------------------------------------------

#### Viewing Your Application

```
$ polymer serve
```

#### Building Your Application

```
$ polymer build
```

This will create builds of your application in the `build/` directory, optimized to be served in production. You can then serve the built versions by giving `polymer serve` a folder to serve from:

```
$ polymer serve build/default
```

#### Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
