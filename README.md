**********************************************************
#  GET /me
```
*  {
*    "first_name": "Irene",
*    "last_name": "Rodríguez",
*    "email": "i.rodriguez.blanco@bbva.com",
*    "mobile": "+34 679 178 553",
*    "company": "BBVA",  
*    "address": "C/ María Tubau 10, 28050 Madrid"
*  }
```
**********************************************************

**********************************************************
#  Objetivo y Alcance del proyecto                   
**********************************************************
```
Aplicar y asimilar los conceptos y tecnologías que nos han
explicado durante las 4 semanas de curso presencial
“Practitioner Bootcamp”, construyendo una aplicación web
que cumpla las siguientes características:

* Temática: listado y creación de movimientos de una cuenta bancaria
* Contará con una base de datos en MongoDB con colecciones de datos que permitan:
  -> Gestionar usuarios con acceso a la aplicación
  -> Gestionar movimientos de las ‘cuentas bancarias’
* Consumirá servicios REST de MLab para acceso y modificación de las colecciones
  en MongoDB
  El consumo de estos servicios REST de MLab será a través de una API propia,
  creando los endpoints necesarios según la funcionalidad que se desarrolle
  para la aplicación

* La funcionalidad y Front se desarrollará con componentes Polymer propios
  o reciclados, HTML5, CSS y Javascript
* Deberá mostrar datos de al menos una API externa
* El código fuente estará disponible en repositorios BitBucket o GitHub
* Funcionará dentro de un contenedor Docker

* Entorno de desarrollo: máquina virtual proporcionada al inicio del curso
* Entorno de ejecución: navegador Chromium instalado en la máquina virtual
  proporcionada al inicio del curso, con las dependencias necesarias para
  ejecutar las tecnologías utilizadas (Git, Node, Polymer, Docker...)
```
**********************************************************
#  Antecedentes personales y expectativas con TechU  
**********************************************************
```
Licenciada en Ingeniería Informática por la Universidad Autónoma de Madrid
en 2005, mi experiencia laboral previa al BBVA fue realizando informes y
cuadros de mando en un equipo de Business Intelligence con herramientas
del mercado como Cognos y Microsoft, con lo que estaba en contacto con
bases de datos pero no con tecnologías de programación.

Mi entrada en BBVA fue para ayudar en el equipo de Riesgos, realizando análisis
para evolutivos y cambios normativos en las aplicaciones HOST de Inversión
Irregular y Mora con procesos batch y cesión de ficheros a terceros, pero sin
 Front, ni (nuevamente) tener contacto con la programación (más allá de leer
   los programas en cobol para entender el alcance de los cambios, pero
   no modificándolos).

Con ganas de conocer proyectos distintos, pedí un cambio y empecé a trabajar
en el equipo que desarrollaba los procesos BPM para usuarios de Oficinas y
Backoffice (Opplus), junto con el mantenimiento de la aplicación de la Cesta
de Tareas (donde se reflejan las tareas de dichos procesos). En este caso la
tecnología Front estaba basada en Nácar y (a veces) Spring  + el BPM de
Software AG, pero… nuevamente dedicada a tareas de gestión de proyectos,
 no estaba a mi alcance aprender ni las herramientas ni la tecnología de
 nuestros proyectos.

Y finalmente, en mi etapa más reciente y por motivos de carga de trabajo
de otros equipos, estoy ahora a cargo de varias aplicaciones destinadas a
los gestores de Oficinas: las Fichas de Cliente, los cuadros de mando “HOY”
que se abre por defecto cada día al entrar en la intranet de Escenia, el
Portal de Contratación, Agenda Única… aplicaciones Front Nácar y alguna en
 WFL… de nuevo gestionando y mucho mantenimiento de aplicaciones en continua
  evolución, con tecnologías distintas a mi experiencia anterior.

Con este curso de BBVA Tech University busco encontrar esa motivación y
entusiasmo que recuerdo que se siente cuando programas y aplicas la tecnología
 ante problemas o necesidades. Volver a pensar, a enfrentarte a una dificultad
 y encontrar la forma de solventarla. Como cuando completas un rompecabezas o
 terminas un crucigrama… ;)

Me gusta la tecnología, quiero aprender y entender los nuevos paradigmas de
la programación y tecnología actual. ¿Sentirme informática de nuevo?
```
