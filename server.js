// ----------------------------------------------------------------------------
// definimos puerto de escucha
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

// definimos bodyParser para poder interpretar el body
var bodyParser = require('body-parser');

//variables para trabajar con MLAB
var baseMlabURL= "https://api.mlab.com/api/1/databases/irb_project/collections/";
var mLabAPIKey ="apiKey=w6OneIggl-HKBf_JyeOcIT6CzhRrtbiS";
var requestJson =require("request-json");

// ----------------------------------------------------------------------------
app.use(bodyParser.json());

app.listen(port);
console.log("API irb_project escuchando en el puerto: "+ port);

// ----------------------------------------------------------------------------
// ---- ENDPOINTS irb_project/v1          -------------------------------------
// ----------------------------------------------------------------------------
// + GET /irb_project/v1
//
// --- API colección client (clientes)
// + GET /irb_project/v1/clients
// + GET /irb_project/v1/clients/:id
// + POST /irb_project/v1/client
// + POST /irb_project/v1/login
// + POST /irb_project/v1/logout/:id
// + DELETE /irb_project/v1/clients/:id

// --- API colección account (cuentas)
// + GET /irb_project/v1/clients/:id/accounts
// + POST /irb_project/v1/clients/:id/account
// + DELETE /irb_project/v1/accounts/:id
// + DELETE /irb_project/v1/clients/:id/accounts
// + PUT /irb_project/v1/accounts/:id/balance/:qty

// --- API colección transactions_account (movimientos)
// + GET /irb_project/v1/accounts/:id/transactions
// + POST /irb_project/v1/accounts/:id/transactions
// + GET /irb_project/v1/accounts/:id/balance
// + DELETE /irb_project/v1/accounts/:id/transactions
// + DELETE /irb_project/v1/clients/:id/transactions

// ----------------------------------------------------------------------------

/*******************************************************************************
// Bienvenida a la API v1
*******************************************************************************/
app.get('/irb_project/v1',
  function(req, res){
      console.log("GET /irb_project/v1");

      /* respuesta en formato json: */
      res.send(
        {
          "msg": "Bienvenido a la API de irb_project"
        }
      );
  }
);

/*******************************************************************************
// GET colección client: listar los clientes
*******************************************************************************/
app.get('/irb_project/v1/clients',
  function(req, res) {
    console.log("GET /irb_project/v1/clients");

    //creamos cliente http con nuestra bd mongo
    httpClient = requestJson.createClient(baseMlabURL);

    // lanzamos un GET contra la colección 'client' de mongo y recuperamos todos
    httpClient.get("client?" + mLabAPIKey,
      //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "Error lanzando GET /v1/clients contra mongo"
        }
        //devolvemos respuesta de 'express'
        res.send(response);
      }
    );
  }
);

/*******************************************************************************
// GET colección client: devolver datos de un cliente
// búsqueda por id_client
*******************************************************************************/
app.get('/irb_project/v1/clients/:id',
  function(req, res) {
    console.log("GET /irb_project/v1/clients/:id");
    var id= req.params.id;
    var query= 'q={"id_client": ' + id + '}' ;

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);

    // lanzamos GET contra la colección 'client' de mongo con id_client concreto
    httpClient.get("client?" + query + "&" + mLabAPIKey,
      //resMLab= variable respuesta distinto nombre 'res' porque si no estaríamos sobreescribiendo la del get
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario con id ="+ id
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario con id="+ id+" no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    );
  }
);


/*******************************************************************************
// POST colección client; ALTA nuevo cliente
//    IN (por body en formato json) =
//           first_name
//         + last_name
//         + email
//         + password
//   OUT = id_client asignado (se busca el valor máx y se suma 1)
// (obtener id max = )
// https://api.mlab.com/api/1/databases/irb_project/collections/client?f={"id_client":1, "_id":0}&s={"id_client":-1}&l=1&apiKey=w6OneIggl-HKBf_JyeOcIT6CzhRrtbiS
*******************************************************************************/
app.post('/irb_project/v1/client',
  function(req, res){
    console.log("POST /irb_project/v1/client");

    var query = 'q={"email": "' +req.body.email +'"}';
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("client?" + query + "&" + mLabAPIKey,
      function(err, resMLab, bodyGet) {
        if(!err)
        {
          //Si no hay respuesta=OK, aún no existe un usuario con ese email
          if (bodyGet.length == 0) {
            //recuperar id más alto en BD para asignar [id máx]+1 al nuevo
            var query = 'f={"id_client":1, "_id":0}&s={"id_client":-1}';

            httpClient.get("client?" + query + "&" + mLabAPIKey,
              function(errMax, resMLab, bodyMax) {
                if (bodyMax.length == 0) {
                  var response = {
                    "msg" : "Error al recuperar máx id_client",
                    "id_client": null
                  }
                  res.send(response);
                }
                //Hemos recuperado el id_client máx +1; podremos insertar
                else {
                  var id_new_client = (bodyMax[0].id_client +1);
                  console.log("(max id)+1= " +id_new_client);

                  var bodyQPOST = '{"id_client":'+ id_new_client + ', "first_name":"' + req.body.first_name + '", "last_name":"' + req.body.last_name
                  + '", "email":"' + req.body.email + '", "password":"' + req.body.password + '"}';
                  console.log("body =" +bodyQPOST);

                  // insertamos nuevo cliente con (id max +1)
                  httpClient.post("client?" + mLabAPIKey, JSON.parse(bodyQPOST),
                    function(errPOST, resMLabPOST, bodyPOST) {
                      //control ERR
                      if(resMLabPOST.statusCode != 200){
                        console.log("ERR. POST alta cliente first_name '"+ req.body.first_name +"', statusCode: " + resMLabPOST.statusCode);
                        console.log(errPOST);
                        var response = {
                          "msg" : "Error en ALTA cliente "+ req.body.first_name,
                          "accessGranted": false,
                          "id_client" : null
                        }
                      }
                      // no hay error resMLabPOST.statusCode==200
                      else {
                        console.log("POST alta cliente first_name '"+ req.body.first_name+"' (id "+id_new_client+ "), correcto.");
                        var response = {
                          "msg" : "ALTA correcta. Bienvenid@, "+ req.body.first_name,
                          "accessGranted": true,
                          "id_client" : id_new_client
                        }
                      }
                      res.send(response);
                    }
                  ); // fin httpclient.post insertamos nuevo cliente con (id max +1)

                } // fin else (hemos recuperado id_client+1)
              }
            ); // fin httpclient.get recuperar id_client max
          }
          //Si existe ya un email igual, no puede hacerse el alta nueva
          else {
            var response = {
              "msg" : "Error en ALTA de cliente; ya existe ese email: "+ req.body.email,
              "accessGranted": false,
              "id_client" : null
            }
            res.send(response);
          }
       } // si hay error al intentar encontrar el email
       else {
         var response = {
           "msg" : "Error en ALTA cliente ("+ req.body.email+")",
           "accessGranted": false,
           "id_client" : null
         }
       }
     }
   ); // httpClient.get buscar el email proporcionado
 }
)

/*******************************************************************************
// DELETE colección client: baja de cliente (borrar de la BD)
//  IN = id_client
*******************************************************************************/
app.delete('/irb_project/v1/clients/:id',
  function(req, res){
      console.log("DELETE /irb_project/v1/clients/:id");

      var query = 'q={"id_client": ' + req.params.id + '}';

      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("client?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (body.length == 0) {
            var response = {
              "msg" : "Error al intentar eliminar el cliente (no existe id: "+req.params.id +")",
              "id_client": null
            }
            res.send(response);
          }
          else {
            //console.log("$oid= "+ body[0]._id.$oid);

            httpClient.del("client/"+ body[0]._id.$oid+ "?" + mLabAPIKey,
              function(errDEL, resMLabDEL, bodyDEL) {
                //control ERR
                if(resMLabDEL.statusCode != 200){
                  console.log("Error al intentar eliminar el cliente con id: "+ req.params.id +", statusCode: " + resMLabDEL.statusCode);
                  console.log(errDEL);
                  var response = {
                    "msg" : "Error al intentar eliminar el cliente con id: "+ req.params.id,
                    "id_client": null
                  }
                }
                else {
                  console.log("DELETE cliente id:"+req.params.id + " correcto.");
                  var response = {
                    "msg" : "Cliente "+ req.params.id+" eliminado correctamente.",
                    "id_client": req.params.id
                  }
                }
                res.send(response);
              }
            );
          }
        }
      );
  }
)
/*******************************************************************************
// POST colección client: LOGIN
// 2 parámetros por body = email + password
// Busca email igual al enviado:
//   - Si no está el email, login incorrecto.
//   - Si está el email
//     + y password coincide: login correcto + añadir propiedad "logged" = "true"
//     + si no es igual el password: login incorrecto.
*******************************************************************************/
app.post('/irb_project/v1/login',
 function(req, res) {
   console.log("POST /irb_project/v1/login");
   var email = req.body.email;
   var password = req.body.password;

   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';

   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("client?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "msg" : "Login incorrecto, email y/o passsword no encontrados",
           "accessGranted" : false
         }
         res.send(response);
       } else {
         console.log("Logging correcto, id =" +body[0].id_client);
         query = 'q={"id_client" : ' + body[0].id_client +'}';
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("client?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT logged true");
             var response = {
               "msg" : "Datos correctos. Bienvenid@ "+ body[0].first_name,
               "accessGranted": true,
               "id_client" : body[0].id_client,
               "name": body[0].first_name
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

/*******************************************************************************
// POST colección client: LOGOUT
// búsqueda por id_client. Unset de propiedad logged
*******************************************************************************/
app.post('/irb_project/v1/logout/:id',
 function(req, res) {
   console.log("POST /irb_project/v1/logout/:id");

   var query = 'q={"id_client": ' + req.params.id + '}';

   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("client?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "msg" : "Logout incorrecto, usuario no encontrado",
           "accessGranted": true
         }
         res.send(response);
       } else {
         query = 'q={"id_client": ' + body[0].id_client +'}';
         var putBody = '{"$unset":{"logged":""}}';

         httpClient.put("client?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT logout");
             var response = {
               "msg" : "Hasta pronto "+ body[0].first_name,
               "accessGranted": false,
               "id" : body[0].id_client
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

/*******************************************************************************
// GET colección account: listar las cuentas de un cliente
// búsqueda por id_client
*******************************************************************************/
app.get('/irb_project/v1/clients/:id/accounts',
  function(req, res) {
    console.log("GET /irb_project/v1/clients/:id/accounts");

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);

    var query= 'q={"id_client": ' + req.params.id + '}' ;
    // lanzamos GET contra la colección 'account' de mongo con id_client concreto
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuentas ",
             "id_client": req.params.id
           }
           //res.status(500);
         } else {
           if (body.length > 0) {
             console.log("Cliente "+ req.params.id +" tiene "+body.length +" cuentas");
             response = body;
           } else {
             response = {
               "msg" : "Cliente no encontrado",
               "id_client": req.params.id
             };
             //res.status(404);
           }
         }
         res.send(response);
       }
    );
  }
);

/*******************************************************************************
// GET colección transactions_account: listar movimientos de una cuenta
// búsqueda por id_account
// recuperamos los movimientos ordenados por fecha descendente
*******************************************************************************/
app.get('/irb_project/v1/accounts/:id/transactions',
  function(req, res) {
    console.log("GET /irb_project/v1/accounts/:id/transactions");

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);

    //por defecto, recuperamos los movimientos ordenados por fecha descendente
    var query= 'q={"id_account": ' + req.params.id + '}&s={"date":-1}' ;

    // lanzamos GET contra la colección 'transactions_account' de mongo con id_account concreto
    httpClient.get("transactions_account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           var response = {
             "msg" : "Error obteniendo movimientos de cuenta",
             "id_account": req.params.id
           }
         } else {
          var response = body;
           if (body.length > 0) {
             console.log("Cuenta "+ req.params.id +" tiene "+body.length +" movimientos");
           } else {
             console.log( "La cuenta no existe o no tiene movimientos");
           }
         }
         res.send(response);
       }
    );
  }
);

/*******************************************************************************
// POST colección account: ALTA nueva CUENTA
//   IN = id_client
//   OUT = id_account asignado (se busca el valor máx y se suma 1)
//https://api.mlab.com/api/1/databases/irb_project/collections/account?f={"id_account":1, "_id":0}&s={"id_account":-1}&l=1&apiKey=w6OneIggl-HKBf_JyeOcIT6CzhRrtbiS
*******************************************************************************/
app.post('/irb_project/v1/clients/:id/account',
  function(req, res){
      console.log("POST /irb_project/v1/clients/:id/account");

      //recuperar id cuenta más alto en BD para asignar [id máx]+1 a la nueva
      var query = 'f={"id_account":1, "_id":0}&s={"id_account":-1}';
      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (body.length == 0) {
            var response = {
              "msg" : "Error al recuperar máx id_account",
              "id_client": req.params.id ,
              "id_account": null
            }
            res.send(response);
          }
          else {
            var new_account = (body[0].id_account +1);
            console.log("(max id)+1= " +new_account);

            var bodyQPOST = '{"id_account": '+ new_account + ', "id_client": ' + req.params.id + ', "balance": 0 }';

            httpClient.post("account?" + mLabAPIKey, JSON.parse(bodyQPOST),
              function(errPOST, resMLabPOST, bodyPOST) {
                //control ERR
                if(resMLabPOST.statusCode != 200){
                  console.log("ERR. POST alta cuenta para id_client nuevo '"+req.params.id  +"', statusCode: " + resMLabPOST.statusCode);
                  console.log(errPOST);
                  var response = {
                    "msg" : "Error en ALTA nueva CUENTA de nuevo cliente ("+ req.params.id +")",
                    "id_client": req.params.id ,
                    "id_account" : null
                  }
                }
                else {
                  console.log("POST alta nueva CUENTA nuevo cliente (id cuenta "+ new_account+ ", id cliente "+ req.params.id +")");
                  var response = {
                    "msg" : "ALTA nueva cuenta '"+new_account+ "' (sin movimientos) correcta",
                    "id_client": req.params.id ,
                    "id_account" : new_account
                  }
                }
                res.send(response);
              }
            );
          }
        }
      );
    }
)
/*******************************************************************************
// DELETE colección account: baja de una cuenta (borrar de la BD)
//  IN = id_account
*******************************************************************************/
app.delete('/irb_project/v1/accounts/:id',
  function(req, res){
      console.log("DELETE /irb_project/v1/accounts/:id");

      var query = 'q={"id_account": ' + req.params.id + '}';

      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (body.length == 0) {
            var response = {
              "msg" : "Error al intentar eliminar la cuenta (no existe id: "+req.params.id +")",
              "id_account": null
            }
            res.send(response);
          }
          else {

            httpClient.del("account/"+ body[0]._id.$oid+ "?" + mLabAPIKey,
              function(errDEL, resMLabDEL, bodyDEL) {
                //control ERR
                if(resMLabDEL.statusCode != 200){
                  console.log("Error al intentar eliminar cuenta con id: "+ req.params.id +", statusCode: " + resMLabDEL.statusCode);
                  console.log(errDEL);
                  var response = {
                    "msg" : "Error al intentar eliminar cuenta con id: "+ req.params.id,
                    "id_account": null
                  }
                }
                else {
                  console.log("DELETE cuenta id:"+req.params.id + " correcto.");
                  var response = {
                    "msg" : "Cuenta "+ req.params.id+" eliminada correctamente.",
                    "id_account": req.params.id
                  }
                }
                res.send(response);
              }
            );
          }
        }
      );
  }
)

/*******************************************************************************
// DELETE colección account: eliminar todas las cuentas de un cliente
//  IN = id_client
// https://api.mlab.com/api/1/databases/irb_project/collections/account?q={%22id_client%22:36}&apiKey=w6OneIggl-HKBf_JyeOcIT6CzhRrtbiS
*******************************************************************************/
app.delete('/irb_project/v1/clients/:id/accounts',
  function(req, res){
      console.log("DELETE /irb_project/v1/clients/:id/accounts");

      var query = 'q={"id_client": ' + req.params.id + '}';

      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            var response = {
              "msg" : "Error buscando cuentas del cliente "+req.params.id,
              "id_client": null
            }
            res.send(response);
          }
          else {
            if (body.length > 0) {
              for(var i=0 ; i< body.length; i++)
              {
                //console.log("delete $oid= "+ body[0]._id.$oid);

                httpClient.del("account/"+ body[i]._id.$oid+ "?" + mLabAPIKey,
                  function(errDEL, resMLabDEL, bodyDEL) {
                    //control ERR
                    if(resMLabDEL.statusCode != 200){
                      console.log("Error al intentar eliminar cuenta (i="+ i +"), statusCode: " + resMLabDEL.statusCode);
                      console.log(errDEL);
                      var response = {
                        "msg" : "Error al intentar eliminar cuenta (i="+ i +")" ,
                        "id_client": null
                      }
                      res.send(response);
                    }
                    else {
                      console.log("DELETE cuenta correcto (i="+ i +")");
                    }
                  }
                );
              }
              console.log(body.length+" cuentas del cliente "+ req.params.id+" eliminadas correctamente.");
              var response = {
                "msg" : body.length+" cuentas del cliente "+ req.params.id+" eliminadas correctamente.",
                "id_client": req.params.id
              }
              res.send(response);
            }
            else {
              var response = {
                "msg" : "Cliente "+ req.params.id +" sin cuentas que borrar.",
                "id_client": req.params.id
              };
              res.send(response);
            }
          }
        }
      );
    }
)

/*******************************************************************************
// POST colección transactions_account: ALTA movimiento NUEVO en una cuenta
// búsqueda por id_account
// IN
// (por parámetros)
//         +  id_account
// (por body)
//         +  id_client
//         + date
//         + amount
//         + currency
//         + category
//   OUT = id_transaction asignado (se busca el valor máx y se suma 1)
//https://api.mlab.com/api/1/databases/irb_project/collections/transactions_account?f={"id_transaction":1, "_id":0}&s={"id_transaction":-1}&l=1&apiKey=w6OneIggl-HKBf_JyeOcIT6CzhRrtbiS
*******************************************************************************/
app.post('/irb_project/v1/accounts/:id/transactions',
  function(req, res) {
    console.log("POST /irb_project/v1/accounts/:id/transactions");

    //recuperar id más alto en BD para asignar [id máx]+1 al nuevo movimiento
    var query = 'f={"id_transaction":1, "_id":0}&s={"id_transaction":-1}&l=1';
    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("transactions_account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (body.length == 0) {
          var response = {
            "msg" : "Error al recuperar máx id_transaction",
            "id": 0
          }
          res.send(response);
        }
        else {
          console.log("(max id)+1= " +(body[0].id_transaction +1));
          var bodyQPOST =
             '{"id_transaction":'+ (body[0].id_transaction+1)
             + ', "id_account":' + req.params.id
             + ', "id_client":' + req.body.id_client
             + ', "date":"' + req.body.date
             + '", "amount":' + req.body.amount
             + ', "currency":"' + req.body.currency
             + '", "category":"' + req.body.category + '"}';
            // console.log("body =" +bodyQPOST);

          //TODO: POST meter en un método genérico== cómo controlar asincronismo???

          httpClient.post("transactions_account?" + mLabAPIKey, JSON.parse(bodyQPOST),
            function(errPOST, resMLabPOST, bodyPOST) {
              //control ERR
              if(resMLabPOST.statusCode != 200){
                console.log("ERR. POST alta nuevo mov en cuenta '"+ req.params.id +"', statusCode: " + resMLabPOST.statusCode);
                console.log(errPOST);
                var response = {
                  "msg" : "Error en ALTA MOV para la cuenta "+req.params.id,
                  "id" : 0
                }
              }
              else {
                console.log("POST alta mov con id '"+ (body[0].id_transaction+1)+"' en cuenta "+req.params.id+ ", correcto.");

                var response = {
                  "msg" : "ALTA de MOV correcta en cuenta "+ req.params.id,
                  "id" : (body[0].id_transaction+1)
                }
              }
              res.send(response);
            }
          );

          //---- fin post
        }
      }
    );
  }
);

/*******************************************************************************
// GET colección transactions_account: obtener saldo de una cuenta
// búsqueda por id_account y, si existe, sumar importes de todos los movimientos
*******************************************************************************/
app.get('/irb_project/v1/accounts/:id/balance',
  function(req, res) {
    console.log("GET /irb_project/v1/accounts/:id/balance");

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);

    var query= 'q={"id_account": ' + req.params.id + '}' ;
    // confirmamos que la cuenta :id tiene movimientos (GET colección 'transactions_account')
    httpClient.get("transactions_account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           var response = {
             "msg" : "Error consultando movimientos de cuenta",
             "id_account": req.params.id
           }
           res.send(response);
         } else {
           if (body.length > 0) {
             var balance = 0.00;
             for(var i=0 ; i< body.length; i++)
             {
               balance = balance + body[i].amount;
               console.log("-> "+ balance);
             }
             console.log("Cuenta "+ req.params.id +" tiene saldo: "+ Math.round(balance*100)/100);
             var response = {
               "msg" : "Saldo calculado a partir de movmientos de cuenta",
               "id_account": req.params.id,
               "balance": Math.round(balance*100)/100
             };
             res.send(response);
           }
           else {
             var response = {
               "msg" : "Cuenta no encontrada. No se puede calcular saldo.",
               "id_account": req.params.id
             };
             res.send(response);
           }
         }
       }
    );
  }
);

/*******************************************************************************
// PUT colección account: actualizar saldo de una cuenta
// búsqueda por id_account y, si existe, actualizar importe
// IN (por parámetro) =
//     id_account
//     balance
// OUT = id_account si existía la cuenta y se ha actualizado
*******************************************************************************/
app.put('/irb_project/v1/accounts/:id/balance/:qty',
  function(req, res) {
    console.log("PUT /irb_project/v1/accounts/:id/balance/:qty");

    //creamos cliente http con nuestra bd
    httpClient = requestJson.createClient(baseMlabURL);

    var query= 'q={"id_account": ' + req.params.id + '}' ;
    // confirmamos que la cuenta :id existe (GET colección 'account')
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           var response = {
             "msg" : "Error consultando cuenta",
             "id_account": null
           }
           res.send(response);
         } else {
           if (body.length > 0) {
             var prev_balance= body[0].balance;
             console.log("Cuenta "+ req.params.id +" con saldo: "+ prev_balance);
             query = 'q={"id_account" : ' + req.params.id +'}';
             var putBody = '{"$set":{"balance":' + req.params.qty +'}}';
             httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT) {
                 console.log("Saldo actualizado. Antes: "+prev_balance + ". Ahora: "+ req.params.qty);
                 var response = {
                   "msg" : "Saldo actualizado. Antes: "+prev_balance + ". Ahora: "+ req.params.qty,
                   "id_account": req.params.id
                 }
                 res.send(response);
               }
             );
           } else {
             response = {
               "msg" : "Cuenta no encontrada. No se puede actualizar saldo.",
               "id_account": req.params.id
             };
             res.send(response);
           }
         }
       }
    );
  }
);

/*******************************************************************************
// DELETE colección transactions_account: eliminar todos los movimientos
// de una cuenta. Búsqueda por id_account
*******************************************************************************/
app.delete('/irb_project/v1/accounts/:id/transactions',
  function(req, res){
      console.log("DELETE /irb_project/v1/accounts/:id/transactions");

      var query = 'q={"id_account": ' + req.params.id + '}';

      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("transactions_account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            var response = {
              "msg" : "Error buscando  movimientos de la cuenta "+req.params.id,
              "id_account": null
            }
            res.send(response);
          }
          else {
            if (body.length > 0) {
              for(var i=0 ; i< body.length; i++)
              {
                //console.log("delete $oid= "+ body[0]._id.$oid);

                httpClient.del("transactions_account/"+ body[i]._id.$oid+ "?" + mLabAPIKey,
                  function(errDEL, resMLabDEL, bodyDEL) {
                    //control ERR
                    if(resMLabDEL.statusCode != 200){
                      console.log("Error al intentar eliminar mov (i="+ i +"), statusCode: " + resMLabDEL.statusCode);
                      console.log(errDEL);
                      var response = {
                        "msg" : "Error al intentar eliminar mov (i="+ i +")",
                        "id_account": null
                      }
                      res.send(response);
                    }
                    else {
                      //console.log("DELETE movimiento correcto (i="+ i +")");
                    }
                  }
                );
              }
              console.log(body.length+" movimientos de la cuenta "+ req.params.id+" eliminados correctamente.");
              var response = {
                "msg" : body.length+" movimientos de la cuenta "+ req.params.id+" eliminados correctamente.",
                "id_account": req.params.id
              }
              res.send(response);
            }
            else {
              var response = {
                "msg" : "Cuenta "+ req.params.id +" sin movimientos que borrar.",
                "id_account": req.params.id
              };
              res.send(response);
            }
          }
        }
      );
    }
)

/*******************************************************************************
// DELETE colección transactions_account: eliminar todos los movimientos
// de un cliente. Búsqueda por id_client
*******************************************************************************/
app.delete('/irb_project/v1/clients/:id/transactions',
  function(req, res){
      console.log("DELETE /irb_project/v1/clients/:id/transactions");

      var query = 'q={"id_client": ' + req.params.id + '}';

      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("transactions_account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            var response = {
              "msg" : "Error buscando  movimientos del ciente "+req.params.id,
              "id_client": null
            }
            res.send(response);
          }
          else {
            if (body.length > 0) {
              for(var i=0 ; i< body.length; i++)
              {
                //console.log("delete $oid= "+ body[0]._id.$oid);

                httpClient.del("transactions_account/"+ body[i]._id.$oid+ "?" + mLabAPIKey,
                  function(errDEL, resMLabDEL, bodyDEL) {
                    //control ERR
                    if(resMLabDEL.statusCode != 200){
                      console.log("Error al intentar eliminar mov (i="+ i +"), statusCode: " + resMLabDEL.statusCode);
                      console.log(errDEL);
                      var response = {
                        "msg" : "Error al intentar eliminar mov (i="+ i +")",
                        "id_client": null
                      }
                      res.send(response);
                    }
                    else {
                      //console.log("DELETE movimiento correcto (i="+ i +")");
                    }
                  }
                );
              }
              console.log(body.length+" movimientos del cliente "+ req.params.id+" eliminados correctamente.");
              var response = {
                "msg" : body.length+" movimientos del cliente "+ req.params.id+" eliminados correctamente.",
                "id_client": req.params.id
              }
              res.send(response);
            }
            else {
              var response = {
                "msg" : "Cliente "+ req.params.id +" sin movimientos que borrar.",
                "id_client": req.params.id
              };
              res.send(response);
            }
          }
        }
      );
    }
)
// PRUEBAS //
// -----------------------------------------------------------------------------
// POST sobre colección mlab
// Entrada:
//  - collection: colección mlab sobre la que se realiza el POST
//  - mLabAPIKey: apiKey de la BD
//  - bodyJSONParse: body en formato JSON.parse que aplica sobre el POST
// Salida:
// - statusCode: código de estado que retorna el POST
// -----------------------------------------------------------------------------
function postToMLabCollection(collection, mLabAPIKey, bodyJSONParse){
  console.log("POST sobre colección "+collection+"...");
  var statusCodePost=0;

  httpClient.post(collection + mLabAPIKey, bodyJSONParse,
    function(errPOST, resMLabPOST, bodyPOST) {
      //control ERR
      if(resMLabPOST.statusCode != 200){
        console.log(" con ERROR, statusCode: " + resMLabPOST.statusCode);
        console.log(errPOST);
        return resMLabPOST.statusCode;
      }
      else {
        console.log(" ¡OK!");
        return 200;
      }
      statusCodePost= resMLabPOST.statusCode;
    }
  );
  //¡¡ asíncrono!! ¿cómo controlo???
//  console.log("statusCodePost: " +statusCodePost);
//  return statusCodePost;
}
